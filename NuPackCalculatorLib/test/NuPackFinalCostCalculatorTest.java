
import static org.junit.Assert.*;

import java.util.ArrayList;

import materialMarkups.FoodMarkup;
import materialMarkups.MaterialMarkup;
import materialMarkups.OthersMarkup;
import materialMarkups.PharmaceuticalsMarkup;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import standardMarkups.FlatMarkup;
import standardMarkups.PeopleMarkup;

public class NuPackFinalCostCalculatorTest {
	private NuPackFinalCostCalculator nuPackCalculator;
	
	@Before
	public void initNuPackCalculator() {
		nuPackCalculator = new NuPackFinalCostCalculator( new FlatMarkup(),	new PeopleMarkup());
	}
	
	@After
	public void releaseNuPackCalculator(){
		nuPackCalculator = null;
	}
	
	@Test
	public void testCalculateFinalCostForJob_ThreePeopleAndFood() throws Exception{
		double basePrice = 1299.99;
		int peopleOnJob = 3;
		ArrayList<MaterialMarkup> materialsMarkup = new ArrayList<MaterialMarkup>();
		materialsMarkup.add(new FoodMarkup());
		
		double finalCost = nuPackCalculator.calculateFinalCostForJob(basePrice, peopleOnJob, materialsMarkup);
		assertEquals(1591.58, finalCost, 0.01);
	}
	
	@Test
	public void testCalculateFinalCostForJob_OnePersonAndDrugs() throws Exception{
		double basePrice = 5432.00;
		int peopleOnJob = 1;
		ArrayList<MaterialMarkup> materialsMarkup = new ArrayList<MaterialMarkup>();
		materialsMarkup.add(new PharmaceuticalsMarkup());

		double finalCost = nuPackCalculator.calculateFinalCostForJob(basePrice, peopleOnJob, materialsMarkup);
		assertEquals(6199.81, finalCost, 0.01);
	}	
	
	@Test
	public void testCalculateFinalCostForJob_FourPeopleAndBooks() throws Exception{
		double basePrice = 12456.95;
		int peopleOnJob = 4;
		ArrayList<MaterialMarkup> materialsMarkup = new ArrayList<MaterialMarkup>();
		materialsMarkup.add(new OthersMarkup());

		double finalCost = nuPackCalculator.calculateFinalCostForJob(basePrice, peopleOnJob, materialsMarkup);
		assertEquals(13707.63, finalCost, 0.01);
	}
}
