

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import standardMarkups.PeopleMarkup;

public class PeopleMarkupTest {
	private PeopleMarkup peopleMarkup;
	
	@Before
	public void initFlatMarkup() {
		peopleMarkup = new PeopleMarkup();
	}
	
	@After
	public void releaseFlatMarkup(){
		peopleMarkup = null;
	}
	
	@Test
	public void testGetTotalPeopleMarkup() {
		double basePrice = 1000.00;
		assertEquals(120.00, peopleMarkup.getTotalPeopleMarkup(basePrice, 10), 0.01);
	}
	
	@Test
	public void testGetTotalPeopleMarkup_ZeroPeople() {
		double basePrice = 1000.00;
		assertEquals(0, peopleMarkup.getTotalPeopleMarkup(basePrice, 0), 0.0);
	}
	
}
