import static org.junit.Assert.*;

import java.util.ArrayList;

import materialMarkups.FoodMarkup;
import materialMarkups.MaterialMarkup;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import standardMarkups.FlatMarkup;
import standardMarkups.PeopleMarkup;

public class NuPackFinalCostCalculator_ExceptionsTest {
	private NuPackFinalCostCalculator nuPackCalculator;
	
	@Before
	public void initNuPackCalculator() {
		nuPackCalculator = new NuPackFinalCostCalculator( new FlatMarkup(),	new PeopleMarkup());
	}
	
	@After
	public void releaseNuPackCalculator(){
		nuPackCalculator = null;
	}
	
	@Test(expected = NullPointerException.class)
	public void testNuPackFinalCostCalculator_WhenFlatMarkupIsNull_ShouldThrowPointerNullException(){
		
		try{
			nuPackCalculator = new NuPackFinalCostCalculator(null, new PeopleMarkup());
		}
		catch(NullPointerException ex){
			checkingExceptionMessage(NuPackFinalCostCalculator.FlatMarkupNullExceptionMessage, ex.getMessage());
			throw ex;
		}
	}
	
	@Test(expected = NullPointerException.class)
	public void testNuPackFinalCostCalculator_WhenPeopleMarkupIsNull_ShouldThrowPointerNullException(){
		
		try{
			nuPackCalculator = new NuPackFinalCostCalculator(new FlatMarkup(), null);
		}
		catch(NullPointerException ex){
			checkingExceptionMessage(NuPackFinalCostCalculator.PeopleMarkupNullExceptionMessage, ex.getMessage());
			throw ex;
		}
	}
	
	@Test(expected = NullPointerException.class)
	public void testCalculateFinalCostForJob_WhenAnyMaterialIsNull_ShouldThrowPointerNullException() throws Exception{
		double basePrice = 12456.95;
		ArrayList<MaterialMarkup> materialsMarkup = new ArrayList<MaterialMarkup>();
		materialsMarkup.add(null);
		
		try{
			nuPackCalculator.calculateFinalCostForJob(basePrice, 4, materialsMarkup);
		}
		catch (NullPointerException ex){
			checkingExceptionMessage(NuPackFinalCostCalculator.AnyNullMaterialExceptionMessage, ex.getMessage());
			throw ex;
		}
	}
	
	@Test(expected = Exception.class)
	public void testCalculateFinalCostForJob_WhenBasePriceIsLessOrEqualThanZero_ShouldThrowAnException() throws Exception{
		double basePrice = 0;
		ArrayList<MaterialMarkup> materialsMarkup = new ArrayList<MaterialMarkup>();
		materialsMarkup.add(new FoodMarkup());
		
		try{
			nuPackCalculator.calculateFinalCostForJob(basePrice, 1, materialsMarkup);
		}
		catch(Exception ex){
			checkingExceptionMessage(NuPackFinalCostCalculator.BasePriceLessOrEqualThanZeroExceptionMessage, ex.getMessage());
			throw ex;
		}
	}
	
	@Test(expected = Exception.class)
	public void testCalculateFinalCostForJob_WhenPeopleNumberIsLessThanZero_ShouldThrowAnException() throws Exception{
		double basePrice = 1000;
		int poepleOnJob = -1;
		ArrayList<MaterialMarkup> materialsMarkup = new ArrayList<MaterialMarkup>();
		materialsMarkup.add(new FoodMarkup());
		
		try{
			nuPackCalculator.calculateFinalCostForJob(basePrice, poepleOnJob, materialsMarkup);
		}
		catch(Exception ex){
			checkingExceptionMessage(NuPackFinalCostCalculator.PeopleNumberLessThanZeroExceptionMessage, ex.getMessage());
			throw ex;
		}
	}
	
	private void checkingExceptionMessage(String expectedMessage, String actualMessage) {
		assertTrue("Incorrect Message" , actualMessage.equals(expectedMessage));
	}
}
