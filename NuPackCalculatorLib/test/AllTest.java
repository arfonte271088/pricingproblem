

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)
@Suite.SuiteClasses({
	NuPackFinalCostCalculatorTest.class,
	NuPackFinalCostCalculator_ExceptionsTest.class,
	FlatMarkupTest.class,
	PeopleMarkupTest.class
})

public class AllTest {

}
