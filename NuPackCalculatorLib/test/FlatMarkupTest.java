


import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import standardMarkups.FlatMarkup;

public class FlatMarkupTest {

	private FlatMarkup flatMarkup;
	
	@Before
	public void initFlatMarkup() {
		flatMarkup = new FlatMarkup();
	}
	
	@After
	public void releaseFlatMarkup(){
		flatMarkup = null;
	}
	
	@Test
	public void testGetBasePricePlusFlatMarkup() {
		double basePrice = 1000.00;
		assertEquals(1050.00, flatMarkup.getBasePricePlusFlatMarkup(basePrice), 0.01);
	}

}
