package standardMarkups;

public class FlatMarkup {

	protected double markupPercent() {
		return 5.0;
	}
	
	public double getBasePricePlusFlatMarkup(double basePrice) {
		double totalFlatMarkup = basePrice * markupPercent() / 100;
		return basePrice + totalFlatMarkup;
	}

}
