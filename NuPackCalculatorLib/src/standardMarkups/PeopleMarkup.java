package standardMarkups;

public class PeopleMarkup {

	protected double markupPercent() {
		return 1.2;
	}
	
	public double getTotalPeopleMarkup(double basePrice, int peopleOnJob) {
		double totalPeopleMarkupPercent = peopleOnJob * markupPercent();
		double totalPeopleMarkup = basePrice * totalPeopleMarkupPercent / 100;
		return totalPeopleMarkup;
	}
}
