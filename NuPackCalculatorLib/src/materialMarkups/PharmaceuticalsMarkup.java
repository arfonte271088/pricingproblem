package materialMarkups;

public class PharmaceuticalsMarkup extends MaterialMarkup {

	@Override
	public double markupPercent() {
		return 7.5;
	}

}
