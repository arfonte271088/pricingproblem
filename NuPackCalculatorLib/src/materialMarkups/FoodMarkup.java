package materialMarkups;

public class FoodMarkup extends MaterialMarkup {

	@Override
	public double markupPercent() {
		return 13.0;
	}

}
