package materialMarkups;

public class ElectronicsMarkup extends MaterialMarkup {

	@Override
	public double markupPercent() {
		return 2.0;
	}

}
