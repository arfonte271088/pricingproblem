
import java.util.ArrayList;

import materialMarkups.MaterialMarkup;

import standardMarkups.FlatMarkup;
import standardMarkups.PeopleMarkup;


public class NuPackFinalCostCalculator {
	public static String BasePriceLessOrEqualThanZeroExceptionMessage = "The base price must be greater than zero.";
	public static String PeopleNumberLessThanZeroExceptionMessage = "The number of people must not be negative.";
	public static String AnyNullMaterialExceptionMessage = "Materials must not contains any null material.";
	public static String FlatMarkupNullExceptionMessage = "Flat Markup instance must not be null";
	public static String PeopleMarkupNullExceptionMessage = "People Markup instance must not be null";
	
	private FlatMarkup flatMarkupPercent;
	private PeopleMarkup peopleMarkupPercent;
	
	public NuPackFinalCostCalculator(FlatMarkup flatMarkupPercent, PeopleMarkup peopleMarkupPercent){
		validateFlatMarkup(flatMarkupPercent);
		validatePeopleMarkup(peopleMarkupPercent);
	}
	
	public double calculateFinalCostForJob (double basePriceForJob, int peopleOnJob, ArrayList<MaterialMarkup> materialsForJob) throws Exception {
		validateBasePrice(basePriceForJob);
		validatePeopleNumber(peopleOnJob);
		validateMaterial(materialsForJob);
		
		return calculateFinalCost(basePriceForJob, peopleOnJob, materialsForJob);
	}
	
	private double calculateFinalCost(double basePrice, int peopleOnJob, ArrayList<MaterialMarkup> materialsForJob) {
		double basePricePlusFlatMarkup = flatMarkupPercent.getBasePricePlusFlatMarkup(basePrice);
		double totalPeopleMarkup = peopleMarkupPercent.getTotalPeopleMarkup(basePricePlusFlatMarkup, peopleOnJob);
		double totalMaterialsMarkup = getTotalMaterialMarkup(basePricePlusFlatMarkup, materialsForJob);
		
		return basePricePlusFlatMarkup + totalPeopleMarkup + totalMaterialsMarkup;
	}	
	
	private double getTotalMaterialMarkup(double basePrice, ArrayList<MaterialMarkup> materials) {
		double cumulativeMaterialMarkup = 0;
		for (MaterialMarkup materialMarkup : materials) 
			cumulativeMaterialMarkup += calculateProportion(basePrice, materialMarkup.markupPercent());
		
		return cumulativeMaterialMarkup;
	}
	
	private void validateFlatMarkup(FlatMarkup flatMarkupPercent) throws NullPointerException {
		if(flatMarkupPercent == null)
			throw new NullPointerException(FlatMarkupNullExceptionMessage);
		this.flatMarkupPercent = flatMarkupPercent;
	}
	
	private void validatePeopleMarkup(PeopleMarkup peopleMarkupPercent) throws NullPointerException {
		if(peopleMarkupPercent == null)
			throw new NullPointerException(PeopleMarkupNullExceptionMessage);
		this.peopleMarkupPercent = peopleMarkupPercent;
	}
	
	private void validateBasePrice(double basePrice) throws Exception{
		if(basePrice <= 0)
			throw new Exception(BasePriceLessOrEqualThanZeroExceptionMessage);
	}
	
	private void validatePeopleNumber(int peopleOnJob) throws Exception{
		if(peopleOnJob < 0)
			throw new Exception(PeopleNumberLessThanZeroExceptionMessage);
	}
	
	private void validateMaterial(ArrayList<MaterialMarkup> materialsForJob) 
		throws NullPointerException {
		
		for(MaterialMarkup material : materialsForJob) {
			if(material == null)
				throw new NullPointerException(AnyNullMaterialExceptionMessage);	
		}
	}
		
	private double calculateProportion(double total, double percent) {
		return total * percent / 100;
	}
}
