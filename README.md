This repo contains NuPackCalculator library. This library allows estimating the final cost for one job.

NuPackFinalCostCalculator is the main class in the library. 
It is initialized with FlatMarkup and PeopleMarkup instances. 
The public method calculateFinalCostForJob is used for estimating the final cost.  It receives base price for job, people on job and the list of material markups. 

All tests were done using JUnit4.